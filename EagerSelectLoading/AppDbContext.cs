﻿using EagerSelectLoading.Entities;
using Microsoft.EntityFrameworkCore;

namespace EagerSelectLoading
{
    public class AppDbContext : DbContext
    {
        public DbSet<Teacher> Teachers { get; set; }

        public DbSet<Course> Courses { get; set; }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            optionsBuilder.UseSqlServer("Server=.;Database=LoadingDb;Trusted_Connection=true;");
            
            base.OnConfiguring(optionsBuilder);
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Teacher>().ToTable("Teacher");

            modelBuilder.Entity<Course>().ToTable("Course");

            modelBuilder.Entity<Teacher>().HasMany(c => c.Courses)
                .WithOne(c => c.Teacher)
                .HasForeignKey(c => c.TeacherId);
            
            base.OnModelCreating(modelBuilder);
        }
    }
}
