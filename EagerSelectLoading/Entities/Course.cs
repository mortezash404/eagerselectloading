﻿namespace EagerSelectLoading.Entities
{
    public class Course
    {
        public int Id { get; set; }

        public string Name { get; set; }

        public int Unit { get; set; }

        public int TeacherId { get; set; }
        public Teacher Teacher { get; set; }
    }
}
