﻿using System.Collections.Generic;

namespace EagerSelectLoading.Entities
{
    public class Teacher
    {
        public int Id { get; set; }

        public string Name { get; set; }

        public ICollection<Course> Courses { get; set; }
    }
}