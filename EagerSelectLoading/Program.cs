﻿using System.Collections.Generic;
using System.Linq;
using EagerSelectLoading.Entities;
using Microsoft.EntityFrameworkCore;

namespace EagerSelectLoading
{
    class Program
    {
        static void Main(string[] args)
        {
            var context = new AppDbContext();

            context.Database.EnsureCreated();

            Insert();

            EagerLoding();

            SelectLoding();
        }

        private static void EagerLoding()
        {
            using (var context = new AppDbContext())
            {
                var teachers = context.Teachers.Include(c => c.Courses).ToList();
            }
        }
        
        private static void SelectLoding()
        {
            using (var context = new AppDbContext())
            {
                var teachers = context.Teachers.Select(c => new
                {
                    c.Name, 
                    Courses = c.Courses.Select(s=>s.Name.StartsWith("C"))
                }).ToList();
            }
        }

        private static void Insert()
        {
            using (var context = new AppDbContext())
            {
                context.Teachers.Add(new Teacher
                {
                    Name = "Masoud",
                    Courses = new List<Course>
                    {
                        new Course {Name = "SQL", Unit = 3},
                        new Course {Name = "BI", Unit = 2}
                    }
                });

                context.SaveChanges();
            }
        }
    }
}
